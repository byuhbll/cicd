/** */
package edu.byu.hbll.cicd.api;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import edu.byu.hbll.cicd.Configuration;

/** @author Charles Draper */
@Path("message")
public class MessageService {

  @Inject private Configuration configuration;

  @GET
  public String get() {
    return configuration.getConfig().path("message").asText("no message set");
  }
}
