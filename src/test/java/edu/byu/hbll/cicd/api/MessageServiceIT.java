/** */
package edu.byu.hbll.cicd.api;

import static org.junit.Assert.assertEquals;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import org.junit.Test;

/** @author Charles Draper */
public class MessageServiceIT {

  private Client client = ClientBuilder.newClient();

  /** Test method for {@link edu.byu.hbll.cicd.api.MessageService#get()}. */
  @Test
  public void testGet() {
    String payaraPort = System.getProperty("payara.port");

    Response response =
        client.target("http://localhost:" + payaraPort + "/cicd/message").request().get();

    // Verify that the correct response code was sent.
    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

    // Verify that the correct response body was sent.
    String responseBody = response.readEntity(String.class);

    assertEquals("Hola amigos", responseBody);
  }
}
